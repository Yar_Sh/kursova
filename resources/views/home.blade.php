@extends('layouts.app')

@section('content')
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card bg-dark text-white">
                <div class="card-header">{{ __('Увага!') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Ви ввійшли до облікового запису адміністратора') }}
                        <a href="/admin/room">Перейти до панелі керування</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
