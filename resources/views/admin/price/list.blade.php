@extends('admin.layout')
@section('content')
    <div class="text-center">
    <h2>Прайс</h2>
    </div>
    <div class="row justify-content-center">


    <table class="table table-sm table-dark" style="max-width: 1000px">
        <thead>
        <th cope="col">Ціна</th>
        <th cope="col">Клас</th>
        <th cope="col">Дія</th>
        </thead>
        @foreach ($costs as $cost)
            <tr>
                <td>
                   {{ $cost->cost}}
                </td>
                <td>{{ $rooms_types[$cost->id_class_n]}}</td>
                <td>
                    <a class="btn btn-sm btn btn-info " tabindex="-1" role="button" aria-disabled="true"  href="/admin/price/{{ $cost->id_class_n}}/edit">Ред.</a>

                </td>
            </tr>
        @endforeach
    </table>
    </div>
@endsection
