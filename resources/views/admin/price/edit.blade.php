@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Коригування ціни</h2>
    <form action="/admin/price/{{ $cost->id_class_n}}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Ціна</label>
        <input type="text" name="ct" value="{{$cost->cost}}">
        <br/><br/>
        <p>Клас {{ $rooms_types[$cost->id_class_n] }}</p>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
