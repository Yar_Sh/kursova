@extends('admin.layout')
@section('content')
    <div class="text-center">
    <h2>Список номерів</h2>
    </div>
    <div class="row justify-content-center">


    <table class="table table-sm table-dark" style="max-width: 1000px">
        <thead>
        <th cope="col">Номер</th>
        <th cope="col">Заброньовано з</th>
        <th cope="col">Заброньовано до</th>
        <th cope="col">Клас</th>
        <th cope="col">Дія</th>
        </thead>
        @foreach ($rooms as $room)
            <tr>
                <td>
                    <a href="/rooms/{{ $room->id_number }}">{{ $room->number_of_room}}</a>
                </td>
                <td>{{ $room->reserved_from}}</td>
                <td>{{ $room->reserved_to}}</td>
                <td>{{ $rooms_types[$room->id_class]}}</td>
                <td>
                    <a class="btn btn-sm btn btn-info " tabindex="-1" role="button" aria-disabled="true"  href="/admin/room/{{ $room->id_number}}/edit">Ред.</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/room/{{ $room->id_number }}"method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-danger btn-block">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    </div>
@endsection
