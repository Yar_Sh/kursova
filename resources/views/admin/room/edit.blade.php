@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>

@section('content')
    <h2>Редагування номеру</h2>
    <form action="/admin/room/{{ $room->id_number}}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Номер</label>
        <input type="text" name="nor" value="{{$room->number_of_room}}">
        <br/><br/>
        <label>Заброньовано з</label>
        <input type="date" name="res_from" value="{{$room->reserved_from}}">
        <br/><br/>
        <label>Заброньовано до</label>
        <input type="date" name="res_to" value="{{$room->reserved_to}}">
        <br/><br/>
        <label>Клас</label>
        <select name="cl">
                @foreach($rooms_types as  $clas_id => $clas_number)
                    <option value="{{ $clas_id }}"
                        {{ ( $clas_id == $room->id_class ) ? 'selected' : '' }}>
                        {{ $clas_number}}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input class="btn btn-primary" type="submit" value="Зберегти">
        @if ($room->reserved_to)
        <a href="edit/rem" class="btn btn-danger">Прибрати бронювання</a>
        @endif
    </form>

@endsection
