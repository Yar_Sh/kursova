@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Додати номер</h2>
    <form action="/admin/room" method="POST">
        {{ method_field('POST') }}
        {{ csrf_field() }}
        <label>Номер </label>
        <input type="text" name="nor">
        <br/><br/>
        <label>Заброньовано з</label>
        <input type="date" name="res_from">
        <br/><br/>
        <label>Заброньовано до</label>
        <input type="date" name="res_to">
        <br/><br/>
        <br/><br/>
        <label>Клас</label>
        <select name="clas">
            @foreach($rooms_types as  $clas_id => $clas_number)
                <option value="{{ $clas_id }}">
                    {{ $clas_number}}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
