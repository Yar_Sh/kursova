
<html>
<head>
    <title>Інформаційна система "Готель"</title>
    <link href="/css/app.css" rel="stylesheet">

</head>
<h1>Панель адміністратора</h1>
<body class="p-3 mb-2 bg-dark text-white">
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b>Перейти до</b>
    <ul>
        <li><a href="/admin/room">Cписок номерів</a></li>
        <li><a href="/admin/room/create">Додати номер</a></li>
        <li><a href="/admin/price">Прайс</a></li>
    </ul>
    <br/>

    <ul>
        <li><a href="/">Головна</a></li>
    </ul>
</div>
<div>
    @yield('content')
</div>
</body>
@include('layouts.footer')
</html>
