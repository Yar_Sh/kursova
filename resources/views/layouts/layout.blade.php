<html>
<head>
    <title>Інформаційна система "Готель"</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body class="p-3 mb-2 bg-dark text-white">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

@yield('page_title')
<br/><br/>
<div class="container">
    Основна інформація
    @yield('content')
</div>
<br/>
<br/>
</body>
<li><a href="/home">Керування</a></li>
@include('layouts.footer')
</html>
