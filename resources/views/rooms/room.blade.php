@extends('layouts.layout')
@section('page_title')
    <b>Інформація про номер {{ $room->number_of_room}}</b>
@endsection
@section('content')
    <p>Номер {{ $room->number_of_room }}</p>
    <p>Клас {{ $rooms_types[$room->id_class] }}</p>
    <p>Ціна {{ $room->cost}}</p>
    @if ($room->reserved_to)

    <p>Заброньовано з {{ $room->reserved_from}}</p>
    <p>Заброньовано до {{ $room->reserved_to}}</p>
    @else
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
        Забронювати
    </button>
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content bg-dark text-white text-center align-items-center">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Забронювати</h5>
                </div>
                <form action="/rooms/{{ $room->id_number}}/res" class="" method="get">
                <div class="modal-body">
                    <label>Забронювати з </label>
                    <input type="date" name="res_from" class="bg-dark text-white"  value="{{$room->reserved_from}}"  required>
                    <br/><br/>
                    <label>Забронювати до </label>
                    <input type="date" class="bg-dark text-white" name="res_to" value="{{$room->reserved_to}}"  required>
                    <br/><br/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Скасувати</button>

                    <input type="submit" class="btn btn-primary" value="Підтвердити"/>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    <a class="btn btn-primary" href="/">Повернутися до списку</a>
@endsection

