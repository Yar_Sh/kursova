@extends('layouts.layout')
@section('page_title')
    <div>
        <img style="position: absolute; left: -50px;height:180px; width: 105%; z-index:-10;" src="images/roof.png">
    <div class="text-center">
        <h2>Список номерів</h2>
    </div>

    <div class="text-center align-items-center">
@endsection
@section('content')

    <form method="get" action="/">
        <select name=" clas" class="form-select form-select-lg mb-3 bg-dark text-white" aria-label=".form-select-lg example">
            <option value="0">всі категорії</option>
            @foreach($room_types as $clas_id => $clas_number)
                <option value="{{ $clas_id }}"
                    {{ ( $clas_id == $clas_selected ) ? 'selected' : '' }}>
                    {{ $clas_number }}
                </option>
            @endforeach
        </select >
        Дата <input type="date" name="res_to" class="bg-dark text-white" value="{{$res_to}}">
        <input type="submit" class="btn btn-primary" value="Знайти" />
        <a href="/" class="btn btn-primary">Cкинути</a>

    </form>
    </div>
    </div>
    <div class="w-120" style="background: url('images/wall(2).png'); background-position: left; background-size: 200px 200px;
    background-repeat: repeat";>
    <div class="row justify-content-center w-75 mx-auto" >

        @foreach ($rooms as $room)
            <div class="d-inline-block " style="position: relative; margin: 15px;"><a  href="/rooms/{{ $room->id_number }}">
                    <img src="images/Lable_number.png" style="height: 25px;
                     width:50px;position: absolute; left:57px; top:75px; opacity:.5;">
                    <div class="text-dark font-weight-bold" STYLE="position: absolute; left:69px; top:76px;
                    z-index: 1000; ">{{ $room->number_of_room}}</div>
                    <img style="height: 300px; width: 160px;" src="images/door.jpg">
                    @if ($room->reserved_to)
                        <img style="width: 35px; position: absolute; left: 4px; top: 161.1px;" src="images/do_not_disturb45.png">
                    @endif
                </a></div>
        @endforeach
    </div>
@endsection
    </div>
