<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class everyMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'date:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deleting the booking expiration date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::update('Update rooms set reserved_to = NULL,reserved_from = NULL
Where reserved_to < CURRENT_DATE()');

    }
}
