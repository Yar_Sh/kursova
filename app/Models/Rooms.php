<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rooms extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_number';
    public $timestamps = false;

    public static $rooms_types = array(
        '1' => 'Стандарт',
        '2' => 'Напівлюкс',
        '3' => 'Люкс',
        '4' => 'Двомісний',
        '5' => 'Багатомісний',
        '6' => 'Президентський',
    );


    public function getRooms($clas,$res_to)
    {
        $query = DB::table('rooms','n');
        $query->select('*', 'cost.cost','room_class.room_class' )
            ->join('cost','n.id_class', '=','cost.id_class_n')
            ->join('room_class','n.id_class', '=','room_class.id_classnum')
            ->orderByDesc('reserved_to');
        if($clas){
            $query->where('id_class', '=', $clas);
        }

        if($res_to){
            $query->where('reserved_to', '<', $res_to)
                ->orWhereNull('reserved_to');
        }
        $rooms = $query->get();
        return $rooms;
    }


    public function getRoomByID($id){
        if(!$id) return null;
        $room = DB::table('rooms','n')
            ->select('*')
            ->join('cost','n.id_class', '=','cost.id_class_n')
            ->where('id_number', $id)
            ->get()->first();
        return $room;
    }


}
