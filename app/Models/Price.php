<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Price extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_class_n';
    protected $table = 'cost';
    public $timestamps = false;
    public static $rooms_types = array(
        '1' => 'Стандарт',
        '2' => 'Напівлюкс',
        '3' => 'Люкс',
        '4' => 'Двомісний',
        '5' => 'Багатомісний',
        '6' => 'Президентський',
    );
}
