<?php

namespace App\Http\Controllers;

use App\Models\Rooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class RoomsController extends Controller
{
    public function index(Request $request){
        $clas = $request->input('clas', null);
        $res_to = $request->input('res_to', null);
        $res_from = $request->input('res_from', null);
        $model_rooms = new Rooms();
        $rooms= $model_rooms->getRooms($clas, $res_to, $res_from);
        return view('rooms.list', [
                'rooms' => $rooms,
                'room_types' => Rooms::$rooms_types,
                'clas_selected' => $clas,
                'res_to' => $res_to,
                'res_from' => $res_from]
        );
    }
    public function room($id){
        $model_rooms = new Rooms();
        $room = $model_rooms->getRoomByID($id);
        return view('rooms.room',['rooms_types' => Rooms::$rooms_types])->with('room', $room );
    }
    public function res(Request $request, $id){

        $room = Rooms::where('id_number', $id)->first();
        $room->reserved_from = $request->input('res_from');
        $room->reserved_to = $request->input('res_to');
        $room->save();
        return Redirect::to('/');
    }
    public function remove_res( $id)
    {
        DB::table('rooms')
            ->where('id_number', $id)
            ->update(['reserved_to' => NULL, 'reserved_from' => NULL]
            );
        return Redirect::to('admin/room/'.$id.'/edit');
    }

}
