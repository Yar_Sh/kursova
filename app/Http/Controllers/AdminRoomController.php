<?php

namespace App\Http\Controllers;
use App\Models\Rooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms= Rooms::get();
        return view('admin.room.list', ['rooms' => $rooms,'rooms_types' => Rooms::$rooms_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.room.add', ['rooms_types' => Rooms::$rooms_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nor = $request->input('nor');
        $clas = $request->input('clas');
        $res_from= $request->input('res_from');
        $res_to= $request->input('res_to');
        $room = new Rooms();
        $room->number_of_room = $nor;
        $room->id_class = $clas;
        $room->reserved_to = $res_to;
        $room->reserved_from = $res_from;
        $room->save();
return Redirect::to('/admin/room');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Rooms::where('id_number', $id)->first();
        return view('admin.room.edit', [
            'room' => $room,
            'rooms_types' => Rooms::$rooms_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $room = Rooms::where('id_number', $id)->first();
        $room->number_of_room = $request->input('nor');
        $room->id_class = $request->input('cl');
        $room->reserved_from = $request->input('res_from');
        $room->reserved_to = $request->input('res_to');
        $room->save();
        return Redirect::to('/admin/room');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Rooms::destroy($id);
        return Redirect::to('/admin/room');
    }
}
