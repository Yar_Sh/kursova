<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminPriceController;
use App\Http\Controllers\RoomsController;
use App\Http\Controllers\AdminRoomController;
use Illuminate\Support\Facades\Auth;;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/',[RoomsController::class, 'index']);
    Route::get('rooms/{room}',[RoomsController::class, 'room']);
    Route::get('rooms/{room}/res',[RoomsController::class, 'res']);
    Route::get('admin/room/{room}/edit/rem',[RoomsController::class, 'remove_res']);
    Route::resource('/admin/room', AdminRoomController::class);
    Route::resource('admin/price', AdminPriceController::class);

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
